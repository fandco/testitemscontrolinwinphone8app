﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Specialized;

namespace TestItemsControlInWinPhone8App
{
    class LotsOfThings : IList<Thing>, INotifyCollectionChanged
    {
        private List<Thing> _things = new List<Thing>();
        public List<Thing> Things
        {
            get {
                return _things;
            }
            set { }
        }

        public LotsOfThings( int pNumberOfThings)
        {
            for( int x = 0; x < pNumberOfThings; x++){
                _things.Add( new Thing());
            }
            //OnCollectionChanged( new NotifyCollectionChangedEventArgs( NotifyCollectionChangedAction.Add));
        }

        #region INotifyCollectionChanged
        public event NotifyCollectionChangedEventHandler CollectionChanged;
        private void OnCollectionChanged(NotifyCollectionChangedEventArgs e)
        {
            NotifyCollectionChangedEventHandler handlers = this.CollectionChanged;
            if (handlers != null)
            {
                foreach (NotifyCollectionChangedEventHandler handler in handlers.GetInvocationList())
                {
                    if (handler.Target is ICollectionView)
                        ((ICollectionView)handler.Target).Refresh();
                    else
                        handler(this, e);
                }
            }
        }
        #endregion

        #region INotifyPropertyChanged
        //public event PropertyChangedEventHandler PropertyChanged;
        //protected void OnPropertyChanged(string pName)
        //{
        //    System.ComponentModel.PropertyChangedEventHandler handler = PropertyChanged;
        //    if (handler != null)
        //    {
        //        handler(this, new PropertyChangedEventArgs(pName));
        //    }
        //}
        #endregion

        #region IList<T> methods
        public int IndexOf(Thing item)
        {
            return _things.IndexOf(item);
        }

        public void Insert(int index, Thing item)
        {
            _things.Insert(index, item);
        }

        public void RemoveAt(int index)
        {
            _things.RemoveAt(index);
        }

        public Thing this[int index]
        {
            get
            {
                return _things[index];
            }
            set
            {
                _things[index] = value;
            }
        }

        public void Add(Thing item)
        {
            _things.Add(item);

            var action = new NotifyCollectionChangedEventArgs( NotifyCollectionChangedAction.Reset);

            OnCollectionChanged( action);
        }

        public void Clear()
        {
            _things.Clear();
        }

        public bool Contains(Thing item)
        {
            return _things.Contains(item);
        }

        public void CopyTo(Thing[] array, int arrayIndex)
        {
            _things.CopyTo(array, arrayIndex);
        }

        public int Count
        {
            get { return _things.Count; }
        }

        public bool IsReadOnly
        {
            get { return false; }
        }

        public bool Remove(Thing item)
        {
            return _things.Remove(item);
        }

        public IEnumerator<Thing> GetEnumerator()
        {
            return _things.GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return _things.GetEnumerator();
        }
        #endregion
    }

    /*
    public class RangeObservableCollection<T> : ObservableCollection<T>
    {
        private bool _SuppressNotification;

        public override event NotifyCollectionChangedEventHandler CollectionChanged;

        protected virtual void OnCollectionChangedMultiItem(
            NotifyCollectionChangedEventArgs e)
        {
            NotifyCollectionChangedEventHandler handlers = this.CollectionChanged;
            if (handlers != null)
            {
                foreach (NotifyCollectionChangedEventHandler handler in
                    handlers.GetInvocationList())
                {
                    if (handler.Target is CollectionView)
                        ((CollectionView)handler.Target).Refresh();
                    else
                        handler(this, e);
                }
            }
        }

        protected override void OnCollectionChanged(NotifyCollectionChangedEventArgs e)
        {
            if (!_SuppressNotification)
            {
                base.OnCollectionChanged(e);
                if (CollectionChanged != null)
                    CollectionChanged.Invoke(this, e);
            }
        }

        public void AddRange(IEnumerable<T> list)
        {
            if (list == null)
                throw new ArgumentNullException("list");

            _SuppressNotification = true;

            foreach (T item in list)
            {
                Add(item);
            }
            _SuppressNotification = false;

            OnCollectionChangedMultiItem(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, list));
        }
    }
     */
}
