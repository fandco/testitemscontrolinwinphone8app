﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using TestItemsControlInWinPhone8App.Resources;

namespace TestItemsControlInWinPhone8App
{
    public partial class MainPage : PhoneApplicationPage
    {
        // Constructor
        public MainPage()
        {
            InitializeComponent();

            this.DataContext = new LotsOfThings(5);
        }

        private void AddSomeThing_Click(object sender, RoutedEventArgs e)
        {
            LotsOfThings lot = this.DataContext as LotsOfThings;

            lot.Add(new Thing());
        }
    }
}