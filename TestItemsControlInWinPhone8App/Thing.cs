﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestItemsControlInWinPhone8App
{
    public class Thing : INotifyPropertyChanged
    {
        private string _Id = Guid.NewGuid().ToString();
        public string Id
        {
            get
            {
                return _Id;
            }
            set { }
        }

        #region Constructor
        public Thing()
        {
            this.OnPropertyChanged( "Id");
        }
        #endregion

        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string pPropertyName)
        {
            System.ComponentModel.PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(pPropertyName));
            }
        }
        #endregion
    }
}
